import { lazy, Suspense } from "react";
import Loader from "components/reusable/loader";

const Header = lazy(() => import("components/header"));
const Footer = lazy(() => import("components/footer"));

function Home() {
  const MainComponent = () => (
    <Suspense fallback={<Loader />}>
      <Header />
      <Footer />
    </Suspense>
  );

  return MainComponent();
}

export default Home;
