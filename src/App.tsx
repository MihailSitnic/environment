import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "pages/home";
import Animation from "components/reusable/animation";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
      </Switch>

      <Animation />
    </Router>
  );
}

export default App;
