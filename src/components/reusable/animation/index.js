import { useEffect } from "react";
import { Link } from "react-scroll";
import WOW from "wow.js";

function Animation() {
  useEffect(() => {
    const wow = new WOW();
    wow.init();
  }, []);

  return (
    <>
      <Link
        to="scroll-to"
        spy={true}
        smooth={true}
        offset={-200}
        duration={500}
        id="scroll"
      />
    </>
  );
}

export default Animation;
