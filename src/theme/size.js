const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobile: "440px",
  mobileL: "768px",
  tablet: "768px",
  tabletL: "950px",
  laptop: "1200px",
  laptopL: "1400px",
  desktopS: "2200px",
  desktop: "2560px",
  tv: "3500px",
};

export default size;
