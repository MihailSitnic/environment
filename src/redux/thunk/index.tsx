import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import API from "redux/api";

export const getInTouch = createAsyncThunk(
  "getInTouch",
  async (val: any) => {
    const body = {
      userName: val.userName,
    };

    const res = await axios.post(`${API}/`, body);
  }
);
