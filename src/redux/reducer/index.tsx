import { createSlice } from "@reduxjs/toolkit";

const rootSlice = createSlice({
  name: "root",
  initialState: {
    userName: "",
  },
  reducers: {
    userName: (state, action) => {
      state.userName = action.payload;
    },
  },
});

export const reducer = rootSlice.reducer;

export type RootState = ReturnType<typeof reducer>;

export const { userName } = rootSlice.actions;
